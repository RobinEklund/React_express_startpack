<h1>App Info</h1>

<h2>Authors</h2>
Robin Eklund

<h2>Version</h2>
1.0.0

<h2>License</h2>
This project is licensed under the MIT License

This is a React starterpack to get started with Express

Please note that there is no database connection made, Data taken is hard-coded in server.js